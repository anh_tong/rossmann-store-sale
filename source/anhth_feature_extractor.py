__author__ = 'anhth'

from _base_feature_extractor import BaseFeatureExtractor

import pandas as pd
from sklearn import preprocessing


class MyFeatureExtractor(BaseFeatureExtractor):

    def extract(self, file_data, file_store, is_train = True):
        le = preprocessing.LabelEncoder()
        enc=preprocessing.OneHotEncoder()
        if is_train:
            #Read train file
            data = pd.read_csv(file_data, parse_dates=[2])
        else:
            #Read test file
            data = pd.read_csv(file_data, parse_dates=[3])

        #Read store file
        store = pd.read_csv(file_store)

        #file missing data with one
        data.fillna(1, inplace=True)

        #ignore the closed store
        data = data[data["Open"] != 0]

        #merge with store feature
        data = pd.merge(data, store, on= "Store")

        #after merging with store, there are alot of NA in Store
        data.fillna(0, inplace=True)

        data["SchoolHoliday"] = data["SchoolHoliday"].astype(float)


        le.fit(data['StateHoliday'])
        x_state_holiday=le.transform(data['StateHoliday'])
        data['StateHoliday'] = x_state_holiday.astype(float)

        data['StateHoliday'] = data['StateHoliday'].astype(float)

        data['year'] = data.Date.dt.year
        data['month'] = data.Date.dt.month
        data['day'] = data.Date.dt.day
        data['DayOfWeek'] = data.Date.dt.dayofweek

        for x in ['a', 'b', 'c', 'd']:
            data['StoreType'+x] = data['StoreType'].map(lambda y: 1 if y == x else 0)


        return data



def test_feature_extract():
    ex = MyFeatureExtractor()
    rs = ex.extract('../data/train.csv', '../data/store.csv')
    rs.to_csv('../data/temp.csv')

    print rs

if __name__ == '__main__':
    test_feature_extract()






