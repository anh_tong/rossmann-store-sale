__author__ = 'anhth'


import pandas as pd
import matplotlib.pyplot as plt

train = pd.DataFrame(pd.read_csv('../data/train.csv'))

in_2013 = train.Date.str.startswith('2013')
store_1 = train['Store'] == 4
train_2013 = train[in_2013 & store_1][['Date', 'Sales']]
train['Date'] = pd.to_datetime(train['Date'])
train_2013['Date'] = pd.to_datetime(train_2013['Date'])
# train_2013.plot(x = 'Date', y = 'Sales')
plt.plot_date(train_2013['Date'], train_2013['Sales'], '-b')
plt.show()