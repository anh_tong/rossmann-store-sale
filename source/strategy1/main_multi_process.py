__author__ = 'anhth'


import pandas as pd
from fitter import Fitter
import datetime
import time
import numpy as np


if __name__ == "__main__":

    train = pd.read_csv("../../data/train.csv", parse_dates = [2])
    test = pd.read_csv("../../data/test.csv", parse_dates = [3])
    real_store = pd.read_csv("../../data/store.csv")

    store = train['Store']
    store = store.drop_duplicates()
    num_empty = 0
    id = []
    sales = []
    final_result = None
    start_time = time.time()
    for s in store.values:
        t1 = time.time()
        print("===============================================")
        print("                     %d                        " % s)
        print("===============================================")
        test_s = test[test['Store'] == s]
        if test_s.empty:
            num_empty += 1
        else:
            temp_store = real_store[real_store['Store'] == s]
            fitter = Fitter(name=str(s), train=train[train['Store'] == s], test= test_s, store = temp_store.reset_index())
            a, b = fitter.fit()
            # if (final_result is None):
            #     final_result = ret_df
            # else:
            #     final_result.append(ret_df)
            id = np.append(id, a)
            sales = np.append(sales, b)

        t2 =  time.time() - t1
        print("Store %d --- Time: %d" %(s, t2))


    print("The number of store which is not predicted for is %d" % num_empty)

    pd.DataFrame({"Id": id, "Sales":sales}).to_csv("../../result/submission-21-11-2015-3.csv",  index_label=None,index_col=False,index=False)
    elapsed_time = time.time() - start_time
    print("Total time is %d" % elapsed_time)
    print "Done !!!!!!!!!!!"



