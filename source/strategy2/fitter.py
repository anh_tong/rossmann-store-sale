__author__ = 'anhth'


import pandas as pd
from tranformer import transform_test, transform_train
from patsy import dmatrices
import numpy as np
import statsmodels.api as sm

def rmspe(y, y_pred, **kwargs):
    return - np.sqrt(np.mean((y_pred/y-1) ** 2))

class Fitter:


    def __init__(self, name, train, test, store):
        self.name = name
        self.train = train
        self.test = test
        self.store = store




    def fit(self):

        train = transform_train(self.train, self.store)
        test = transform_test(self.test, self.store)
        formula = "Sales ~ DayOfWeek + Promo + StateHoliday + SchoolHoliday + month + day + year + promo2 + compete"

        #train
        y, X = dmatrices(formula, data = train, return_type = "dataframe")
        mod = sm.OLS(y, X)
        res = mod.fit()
        print(res.summary())

        #test
        test["Sales"] = [0 for x in range(len(test))]
        y_p, real_test = dmatrices(formula, data = test, return_type = 'dataframe')


        x_test = real_test.as_matrix(["t"])
        ym = res.predict(endog =dict(x1=x_test))
        # print X.shape[1]
        # k = pyGps.cov.SM(Q=10)
        # k.initSMhypers(X, y)
        # m = pyGps.mean.Zero()
        # model = pyGps.GPR()
        # model.setPrior(kernel=k)
        # model.optimize(X, y)
        # ym, ys2, fmu, fs2, lp = model.predict(x_test)
        # model.plot()
        #
        return test.as_matrix(["Id"]).T, ym

from operator import itemgetter
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")