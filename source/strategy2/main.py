__author__ = 'anhth'


import pandas as pd
from fitter import Fitter
import datetime
import time


train = pd.read_csv("../../data/train.csv", parse_dates = [2])
test = pd.read_csv("../../data/test.csv", parse_dates = [3])

store = train['Store']
store = store.drop_duplicates()
num_empty = 0
final_result = None
start_time = time.time()
for s in store.values:
    print("===============================================")
    print("                     %d                        " % s)
    print("===============================================")
    test_s = test[test['Store'] == s]
    if test_s.empty:
        num_empty += 1
    else:
        fitter = Fitter(name=str(s), train=train[train['Store'] == s], test= test_s, store = None)
        ret_df = fitter.fit()
        if (final_result is None):
            final_result = ret_df
        else:
            final_result.append(ret_df)


print("The number of store which is not predicted for is %d" % num_empty)


final_result = final_result.sort(['Id'], ascending = True)
final_result.to_csv("../../result/submission" + str(datetime.datetime.now().time()) + ".csv")
elapsed_time = time.time() - start_time
print("Total time is %d" % elapsed_time)
print "Done !!!!!!!!!!!"



