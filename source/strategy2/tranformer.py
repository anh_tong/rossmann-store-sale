__author__ = 'anhth'

from pandas import DataFrame
import pandas as pd
from calendar import month_abbr


class Store:

    def __init__(self, input_df):
        self.df = input_df
        self.df.fillna(0, inplace= True)
        self.month_dict = dict()
        self.month_dict["Jan"] = 1
        self.month_dict["Feb"] = 2
        self.month_dict["Mar"] = 3
        self.month_dict["Apr"] = 4
        self.month_dict["May"] = 5
        self.month_dict["Jun"] = 6
        self.month_dict["Jul"] = 7
        self.month_dict["Aug"] = 8
        self.month_dict["Sept"] = 9
        self.month_dict["Oct"] = 10
        self.month_dict["Nov"] = 11
        self.month_dict["Dec"] = 12
        self.init()


    def init(self):
        a = self.df["CompetitionOpenSinceMonth"]
        self.competive_month = self.df["CompetitionOpenSinceMonth"][0]
        self.competive_year = self.df["CompetitionOpenSinceYear"][0]
        self.promo2_p = self.df["Promo2"][0]
        self.promo2_week = self.df["Promo2SinceWeek"][0]
        self.promo2_year = self.df["Promo2SinceYear"][0]
        self.interval = self.df["PromoInterval"][0]
        temp = []
        if self.interval == 0:
            self.interval = temp
        else:
            for m in self.interval.split(","):
                temp.append(self.month_dict[m])
            self.interval = temp


    def promo2(self, week,year, month):
        if self.promo2_p == 1:
            if year < self.promo2_year:
                return 0
            elif year == self.promo2_year:
                if week < self.promo2_week:
                    return 0
                else:
                    if month not in self.interval:
                        return 0
                    else:
                        return 1
            else:
                if month not in self.interval:
                    return 0
                else:
                    return 1
        else:
            return 0

    def compete(self, month, year):
        if self.competive_month == 0 or self.competive_year == 0:
            return 1
        else:
            if year > self.competive_year:
                return 1
            elif year == self.competive_year:
                if month >= self.competive_month:
                    return 1
                else:
                    return 0
            else:
                return 0


def toBinary(featureCol, df):
    values = set(df[featureCol].unique())
    newCol = [featureCol + val for val in values]
    for val in values:
        df[featureCol + val] = df[featureCol].map(lambda x: 1 if x == val else 0)

def transform_train(data, store=None):

    """
    Transform train data
    :param data: input train data for one store
    :type data: DataFrame
    :return: A transformed data frame
    :rtype: DataFrame
    """

    s = Store(store)

    def promo2(row):
        return s.promo2(row['week'],row['year'], row['month'])

    def compete(row):
        return s.compete(row["month"], row["year"])

    data = data[data['Open'] != 0]

    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1

    data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    data.loc[:,'StateHoliday'] = data['StateHoliday'].astype(float)

    data.loc[:,"year"] = data.Date.dt.year
    data.loc[:,'month'] = data.Date.dt.month
    data.loc[:,'day'] = data.Date.dt.day
    data.loc[:,'week'] = data.Date.dt.week
    data.loc[:, 'compete'] = data.apply(compete, axis = 1)
    data.loc[:, 'promo2'] = data.apply(promo2, axis = 1)


    data = data.drop(["Date"], axis = 1)

    return data


def transform_test(data, store = None):

    """
    Transform train data
    :param data: input train data for one store
    :type data: DataFrame
    :return: A transformed data frame
    :rtype: DataFrame
    """

    s = Store(store)

    def promo2(row):
        return s.promo2(row["week"], row["year"], row["month"])

    def compete(row):
        return s.compete(row["month"], row["year"])

    data = data[data['Open'] != 0]

    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1

    data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    data.loc[:,'StateHoliday'] = data['StateHoliday'].astype(float)

    data.loc[:,"year"] = data.Date.dt.year
    data.loc[:,'month'] = data.Date.dt.month
    data.loc[:,'day'] = data.Date.dt.day
    data.loc[:,'week'] = data.Date.dt.week
    data.loc[:, 'compete'] = data.apply(compete, axis = 1)
    data.loc[:, 'promo2'] = data.apply(promo2, axis = 1)

    data = data.drop(["Date"], axis = 1)


    return data