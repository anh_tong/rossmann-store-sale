__author__ = 'anhth'

import pandas as pd
from sklearn.preprocessing import StandardScaler
from patsy import dmatrices
from operator import itemgetter
from sklearn.grid_search import GridSearchCV
import numpy as np

from sklearn import cross_validation

def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")

def rmspe(y, y_pred, **kwargs):
    e_y = y
    e_y_pred = y_pred
    return - np.sqrt(np.mean((e_y_pred/e_y-1) ** 2))

def toBinary(featureCol, df):
    values = set(df[featureCol].unique())
    newCol = [featureCol + val for val in values]
    for val in values:
        df[featureCol + val] = df[featureCol].map(lambda x: 1 if x == val else 0)

def build_feature(data):
    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1

    data['SchoolHoliday'] = data['SchoolHoliday'].astype(float)
    data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    data['StateHoliday'] = data['StateHoliday'].astype(float)
    data['year'] = data.Date.dt.year
    data['month'] = data.Date.dt.month
    data['day'] = data.Date.dt.day
    data['DayOfWeek'] = data.Date.dt.dayofweek

    for x in ['a', 'b', 'c', 'd']:
        data['StoreType' + x] = data['StoreType'].map(lambda y: 1 if y == x else 0)

    toBinary('Assortment', data)




if __name__ == "__main__":

    train = pd.read_csv("../../data/train.csv", parse_dates = [2])
    test = pd.read_csv("../../data/test.csv", parse_dates = [3])
    store = pd.read_csv("../../data/store.csv")

    train.fillna(1, inplace=True)
    test.fillna(1, inplace=True)

    train = train[train["Open"] != 0]
    train = train[train["Sales"] > 0]

    train = pd.merge(train, store, on='Store')
    test = pd.merge(test, store, on='Store')

    build_feature(train)
    build_feature(test)

    train = train[train["Store"] == 3]
    test = test[test["Store"] == 3]

    formula = "Sales ~ DayOfWeek + Promo + SchoolHoliday + month + day + year"
    y, X = dmatrices(formula, data = train, return_type = "dataframe")
    y = np.asarray(y).ravel()
    test["Sales"] = [0 for x in range(len(test))]
    y_p, real_test = dmatrices(formula, data = test, return_type = 'dataframe')

    from multilayer_perceptron import MLPRegressor

    mlp = MLPRegressor( max_iter=4000, hidden_layer_sizes=(100))

    from sklearn.metrics import mean_squared_error, make_scorer
    mse = make_scorer(rmspe)
    #
    # grid_search = GridSearchCV(mlp,
    #                                param_grid={},
    #                                verbose= 1,
    #                                scoring= mse,
    #                                n_jobs= 8
    #                                # cv = StratifiedShuffleSplit(Y_train, n_iter=10, test_size=0.2, train_size=None,random_state=1000)
    #                                ).fit(X, y)
    #
    # print("Best score: %0.3f" % grid_search.best_score_)
    # print(grid_search.best_estimator_)
    # report(grid_search.grid_scores_)
    scaler = StandardScaler()
    scaler.fit(X)
    X = scaler.transform(X)
    import matplotlib.pyplot as plt

    scores = cross_validation.cross_val_score(mlp, X, y, cv=10, scoring = mse)
    print scores
    mlp.fit(X, y)
    real_test = scaler.transform(real_test)
    predicted = mlp.predict(real_test)

    print predicted
    a = pd.DataFrame({"Id":test["Id"], "Sales": predicted})
    plt.plot(test["Date"], predicted)
    plt.plot(train["Date"], train["Sales"])


    plt.show()









