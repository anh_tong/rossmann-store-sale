__author__ = 'anhth'


import pandas as pd
import numpy as np
from patsy import dmatrices
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.grid_search import GridSearchCV


def create_month_dict():
    month_dict = dict()
    month_dict["Jan"] = 1
    month_dict["Feb"] = 2
    month_dict["Mar"] = 3
    month_dict["Apr"] = 4
    month_dict["May"] = 5
    month_dict["Jun"] = 6
    month_dict["Jul"] = 7
    month_dict["Aug"] = 8
    month_dict["Sept"] = 9
    month_dict["Oct"] = 10
    month_dict["Nov"] = 11
    month_dict["Dec"] = 12


def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")

def rmspe(y, y_pred, **kwargs):
    return - np.sqrt(np.mean((y_pred/y-1) ** 2))

def build_feature(data):
    data.loc[:, "year"] = data.Date.dt.year
    data.loc[:,'month'] = data.Date.dt.month
    data.loc[:,'day'] = data.Date.dt.day
    data.loc[:,'week'] = data.Date.dt.week

    data['CompetitionOpen'] = 12 * (data.year - data.CompetitionOpenSinceYear) + \
	(data.month - data.CompetitionOpenSinceMonth)
    data['CompetitionOpen'] = data.CompetitionOpen.apply(lambda x: x if x > 0 else 0)

    data['PromoOpen'] = 12 * (data.year - data.Promo2SinceYear) + \
	(data.week - data.Promo2SinceWeek) / float(4)
    data['PromoOpen'] = data.PromoOpen.apply(lambda x: x if x > 0 else 0)
    data['p_1'] = data.PromoInterval.apply(lambda x: x[:3] if type(x) == str else 0)
    data['p_2'] = data.PromoInterval.apply(lambda x: x[4:7] if type(x) == str else 0)
    data['p_3'] = data.PromoInterval.apply(lambda x: x[8:11] if type(x) == str else 0)
    data['p_4'] = data.PromoInterval.apply(lambda x: x[12:15] if type(x) == str else 0)

    data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    data['StateHoliday'] = data['StateHoliday'].astype(float)

    data = pd.get_dummies(data, columns = ['p_1', 'p_2', 'p_3', 'p_4',
	                                       'StoreType',
	                                       'Assortment'])
    data = data.fillna(0)
    data = data.sort_index(axis=1)
    return data

month_dict = create_month_dict()

if __name__ == "__main__":
    train = pd.read_csv("../../data/train.csv", parse_dates = [2])
    test = pd.read_csv("../../data/test.csv", parse_dates = [3])
    store = pd.read_csv("../../data/store.csv")

    train.fillna(1, inplace=True)
    test.fillna(1, inplace=True)

    train = train[train["Open"] != 0]
    train = train[train["Sales"] > 0]

    train = pd.merge(train, store, on='Store')
    test = pd.merge(test, store, on='Store')

    train = build_feature(train)
    test = build_feature(test)



    formula = "Sales ~ Store + DayOfWeek + Promo + " \
              "StateHoliday + " \
              "StoreType_a + StoreType_b + StoreType_c + StoreType_d + " \
              "Assortment_a + Assortment_b + Assortment_c + " \
              "SchoolHoliday + " \
              "month + day + year + " \
              "CompetitionOpen + PromoOpen"

    y, X = dmatrices(formula, data = train, return_type = "dataframe")
    print X.columns.values
    y = np.asarray(y).ravel()
    test["Sales"] = [0 for x in range(len(test))]
    y_p, real_test = dmatrices(formula, data = test, return_type = 'dataframe')

    clf= GradientBoostingRegressor()
    # param_grid = {
    #     "n_estimators": [130, 150, 200, 250],
    #     "max_depth": [3, 4,5,6],
    #     "max_features":["auto","sqrt"],
    #     "loss" : ["ls", "lad", "huber"],
    # }

    param_grid = {
        "n_estimators": [130],
        "max_depth": [4],
    }

    from sklearn.metrics import make_scorer
    mse = make_scorer(rmspe)

    grid_search = GridSearchCV(clf,
                                   param_grid=param_grid,
                                   verbose= 1,
                                   scoring= mse,
                                   # n_jobs= 1
                                   # cv = StratifiedShuffleSplit(Y_train, n_iter=10, test_size=0.2, train_size=None,random_state=1000)
                                   ).fit(X, y)

    print("Best score: %0.3f" % grid_search.best_score_)
    print(grid_search.best_estimator_)
    report(grid_search.grid_scores_)



