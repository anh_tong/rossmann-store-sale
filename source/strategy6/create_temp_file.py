__author__ = 'anhth'

from ross_data import my_load_data

(train, test) = my_load_data()
for store in range(1115, 1116):
    dir_train = '../../data/store/train_store_%d.csv'%store
    dir_test = '../../data/store/test_store_%d.csv'%store
    train[train["Store"] == store].to_csv(dir_train, index_label=None,index_col=False,index=False)
    test[test["Store"] == store].to_csv(dir_test, index_label=None,index_col=False,index=False)