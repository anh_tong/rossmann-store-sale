__author__ = 'anhth'



import pandas as pd

def merge_result_files(path, save_file, max = 1115):

    fout = open(save_file, "a")
    first = path + "1.csv"
    for line in open(first):
        fout.write(line)

    for num in range(2,max+1):
        current = path + "%d.csv"%num
        try:
            f = open(current)
        except IOError as e:
            print "No file %s" % current
            continue
        f.next()
        for line in f:
            fout.write(line)
        f.close()

    fout.close()

df = pd.read_csv("../../result/submission-21-11-2015-3.csv")
df.loc[:, "Id"] = df["Id"].astype(int)



df.to_csv("../../result/submission-21-11-2015-3-final.csv", index_label=None,index_col=False,index=False)


# df = pd.read_csv("../../data/test.csv")
#
# df = df[df["Open"] ==0]
#
# df = df.assign(Sales = lambda x: 0.0)
# print(df)
#
# df.to_csv("../../result/submission-21-11-2015-1-0.csv",  columns = ["Id", "Sales"], index_label=None,index_col=False,index=False)