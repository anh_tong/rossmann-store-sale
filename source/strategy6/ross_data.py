__author__ = 'anhth'


import os
import pdb
import sys

import numpy

import theano
from numpy import genfromtxt
from pandas import Series

import sklearn.cross_validation as cv
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler

import pandas as pd
from patsy import dmatrices
import time


def toBinary(featureCol, df):
    values = set(df[featureCol].unique())
    newCol = [featureCol + val for val in values]
    for val in values:
        df[featureCol + val] = df[featureCol].map(lambda x: 1 if x == val else 0)
    return newCol

def create_month_dict():
    month_dict = dict()
    month_dict[1] = "Jan"
    month_dict[2] = "Feb"
    month_dict[3] = "Mar"
    month_dict[4] = "Apr"
    month_dict[5] = "May"
    month_dict[6] = "Jun"
    month_dict[7] = "Jul"
    month_dict[8] = "Aug"
    month_dict[9] = "Sept"
    month_dict[10] = "Oct"
    month_dict[11] = "Nov"
    month_dict[12] = "Dec"
    return month_dict

def build_features(data):
    # remove NaNs
    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1
    # Use some properties directly

    # add some more with a bit of preprocessing
    data['SchoolHoliday'] = data['SchoolHoliday'].astype(float)

    data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    data['StateHoliday'] = data['StateHoliday'].astype(float)

    data['year'] = data.Date.dt.year
    data['month'] = data.Date.dt.month
    data['day'] = data.Date.dt.day
    data['DayOfWeek'] = data.Date.dt.dayofweek
    data.loc[:,'week'] = data.Date.dt.week


    data['CompetitionOpen'] = 12 * (data.year - data.CompetitionOpenSinceYear) + \
	(data.month - data.CompetitionOpenSinceMonth)
    data['CompetitionOpen'] = data.CompetitionOpen.apply(lambda x: x if x > 0 else 0)

    data['PromoOpen'] = 12 * (data.year - data.Promo2SinceYear) + \
	(data.week - data.Promo2SinceWeek) / float(4)
    data['PromoOpen'] = data.PromoOpen.apply(lambda x: x if x > 0 else 0)
    month_dict = create_month_dict()
    def f(row):
        if row['Open'] > 0:
            if row["PromoInterval"] == 0:
                return 0
            if month_dict[row['month']] in row["PromoInterval"]:
                return 1
            else:
                return 0
        else:
            return 0


    data.loc[:, 'PromoOpen'] = data.apply(f, axis = 1)


    for x in ['a', 'b', 'c', 'd']:
        data['StoreType' + x] = data['StoreType'].map(lambda y: 1 if y == x else 0)

    newCol = toBinary('Assortment', data)

def data_preprocessing(data):
    data = data[350:,:]
    # Standarization

    data = data - data.mean(axis=0)
    data = data/data.std(axis=0)
    #Some kind of smoothing??

    #min_max = preprocessing.MinMaxScaler()
    #data = min_max.fit_transform(data)

    #Put between 1 and 0
    return data

def my_load_data():
    print("Loading whole data ...")
    t1 = time.time()
    train = pd.read_csv("../../data/train.csv", parse_dates=[2])
    test = pd.read_csv("../../data/test.csv", parse_dates=[3])
    store = pd.read_csv("../../data/store.csv")

    train.fillna(1, inplace=True)
    test.fillna(1, inplace=True)
    train = train[train["Open"] != 0]
    test = test[test["Open"] != 0]
    train = train[train["Sales"] > 0]

    train = pd.merge(train, store, on='Store')
    test = pd.merge(test, store, on='Store')

    build_features(train)
    build_features(test)
    t2 = time.time() - t1
    print("Finish loading data. Elapse: %d" % t2)
    return train, test


def my_read_data(train, test, store, path="table_a.csv", dir="/home/anhth/course-work/machine-learning/kaggle-competition/rossmann-store-sales/data",
        max_len=30, valid_portion=0.1, columns=4):

    # train = train[train["Store"] == store]
    # test = test[test["Store"] == store]
    # test = test[test["Open"] != 0]

    data_train = train[['Promo', 'Promo2', 'StateHoliday', 'SchoolHoliday', 'CompetitionOpen', 'PromoOpen', 'Sales']]
    scaler = StandardScaler()
    scaler.fit(data_train)
    data_train = scaler.transform(data_train)
    train_set_x = numpy.array([data_train[i:i+max_len,:] for i in xrange(len(data_train)-max_len)])
    train_set_y = numpy.array([data_train[i][0] for i in xrange(max_len , len(data_train))])

    # split training set into validation set
    n_samples = len(train_set_x)
    sidx = numpy.random.permutation(n_samples)
    n_train = int(numpy.round(n_samples * (1. - valid_portion)))
    valid_set_x = [train_set_x[s] for s in sidx[n_train:]]
    valid_set_y = [train_set_y[s] for s in sidx[n_train:]]
    train_set_x = [train_set_x[s] for s in sidx[:n_train]]
    train_set_y = [train_set_y[s] for s in sidx[:n_train]]

    # formula = "Sales ~ Promo + Promo2 + StateHoliday + SchoolHoliday + CompetitionOpen + PromoOpen"
    # y, X = dmatrices(formula, data = train, return_type = "dataframe")
    # y = numpy.asarray(y).ravel()
    test["Sales"] = [0 for x in range(len(test))]
    data_test = test[['Promo', 'Promo2', 'StateHoliday', 'SchoolHoliday', 'CompetitionOpen', 'PromoOpen', 'Sales']]
    data_test = scaler.transform(data_test)

    combine = numpy.append(data_train, data_test, axis= 0)
    test_set_x = numpy.array([combine[i:i+max_len,:] for i in xrange(len(combine) - len(data_test) - max_len, len(combine) - max_len)])

    # test_set_x = numpy.array([data_test[i:i+max_len,:] for i in xrange(len(data_test)-max_len)])
    # test_set_y = numpy.array([data_test[i][0] for i in xrange(max_len , len(data_test))])
    test_set_y = None
    # y_p, real_test = dmatrices(formula, data = test, return_type = 'dataframe')


    # path = os.path.join(dir, path)
    #
    # data = genfromtxt(path, delimiter=',')
    #
    # data = data[:,2:(2+columns)]
    #
    # data = data_preprocessing(data)
    #
    # x_data = numpy.array([data[i:i+max_len,:] for i in xrange(len(data)-max_len)])
    # y_data = numpy.array([data[i][0] for i in xrange(max_len , len(data))])
    #
    # # split data into training and test
    # train_set_x, test_set_x, train_set_y, test_set_y = cv.train_test_split(x_data,
    # y_data, test_size=0.3, random_state=0)
    #
    # # split training set into validation set
    # n_samples = len(train_set_x)
    # sidx = numpy.random.permutation(n_samples)
    # n_train = int(numpy.round(n_samples * (1. - valid_portion)))
    # valid_set_x = [train_set_x[s] for s in sidx[n_train:]]
    # valid_set_y = [train_set_y[s] for s in sidx[n_train:]]
    # train_set_x = [train_set_x[s] for s in sidx[:n_train]]
    # train_set_y = [train_set_y[s] for s in sidx[:n_train]]

    train_set = (train_set_x, train_set_y)
    valid_set = (valid_set_x, valid_set_y)

    train = (train_set_x, train_set_y)
    valid = (valid_set_x, valid_set_y)
    test = (test_set_x, test_set_y)

    return train, valid, test, scaler

def read_data(path="table_a.csv", dir="/home/anhth/course-work/machine-learning/kaggle-competition/rossmann-store-sales/data",
        max_len=30, valid_portion=0.1, columns=4):

    train = pd.read_csv("../../data/train.csv", parse_dates=[2])
    test = pd.read_csv("../../data/test.csv", parse_dates=[3])
    store = pd.read_csv("../../data/store.csv")

    train.fillna(1, inplace=True)
    test.fillna(1, inplace=True)
    train = train[train["Open"] != 0]
    train = train[train["Sales"] > 0]

    train = pd.merge(train, store, on='Store')
    test = pd.merge(test, store, on='Store')

    build_features(train)
    build_features(test)

    train = train[train["Store"] == 3]
    test = test[test["Store"] == 3]

    data_train = train.as_matrix(['Promo', 'Promo2', 'StateHoliday', 'SchoolHoliday', 'CompetitionOpen', 'PromoOpen', 'Sales'])
    scaler = StandardScaler()
    scaler.fit(data_train)
    data_train = scaler.transform(data_train)
    train_set_x = numpy.array([data_train[i:i+max_len,:] for i in xrange(len(data_train)-max_len)])
    train_set_y = numpy.array([data_train[i][0] for i in xrange(max_len , len(data_train))])

    # split training set into validation set
    n_samples = len(train_set_x)
    sidx = numpy.random.permutation(n_samples)
    n_train = int(numpy.round(n_samples * (1. - valid_portion)))
    valid_set_x = [train_set_x[s] for s in sidx[n_train:]]
    valid_set_y = [train_set_y[s] for s in sidx[n_train:]]
    train_set_x = [train_set_x[s] for s in sidx[:n_train]]
    train_set_y = [train_set_y[s] for s in sidx[:n_train]]

    # formula = "Sales ~ Promo + Promo2 + StateHoliday + SchoolHoliday + CompetitionOpen + PromoOpen"
    # y, X = dmatrices(formula, data = train, return_type = "dataframe")
    # y = numpy.asarray(y).ravel()
    test["Sales"] = [0 for x in range(len(test))]
    data_test = test.as_matrix(['Promo', 'Promo2', 'StateHoliday', 'SchoolHoliday', 'CompetitionOpen', 'PromoOpen', 'Sales'])
    data_test = scaler.transform(data_test)
    test_set_x = numpy.array([data_test[i:i+max_len,:] for i in xrange(len(data_test)-max_len)])
    test_set_y = numpy.array([data_test[i][0] for i in xrange(max_len , len(data_test))])
    # y_p, real_test = dmatrices(formula, data = test, return_type = 'dataframe')


    # path = os.path.join(dir, path)
    #
    # data = genfromtxt(path, delimiter=',')
    #
    # data = data[:,2:(2+columns)]
    #
    # data = data_preprocessing(data)
    #
    # x_data = numpy.array([data[i:i+max_len,:] for i in xrange(len(data)-max_len)])
    # y_data = numpy.array([data[i][0] for i in xrange(max_len , len(data))])
    #
    # # split data into training and test
    # train_set_x, test_set_x, train_set_y, test_set_y = cv.train_test_split(x_data,
    # y_data, test_size=0.3, random_state=0)
    #
    # # split training set into validation set
    # n_samples = len(train_set_x)
    # sidx = numpy.random.permutation(n_samples)
    # n_train = int(numpy.round(n_samples * (1. - valid_portion)))
    # valid_set_x = [train_set_x[s] for s in sidx[n_train:]]
    # valid_set_y = [train_set_y[s] for s in sidx[n_train:]]
    # train_set_x = [train_set_x[s] for s in sidx[:n_train]]
    # train_set_y = [train_set_y[s] for s in sidx[:n_train]]

    train_set = (train_set_x, train_set_y)
    valid_set = (valid_set_x, valid_set_y)

    train = (train_set_x, train_set_y)
    valid = (valid_set_x, valid_set_y)
    test = (test_set_x, test_set_y)

    return train, valid, test

def prepare_data(seqs, labels, steps, x_dim):
    n_samples = len(seqs)
    max_len = steps
    x = numpy.zeros((max_len, n_samples, x_dim)).astype('float32')
    y = numpy.asarray(labels, dtype='float32')

    for idx, s in enumerate(seqs):
        x[:,idx,:] = s

    return x, y

def my_prepare_data(seqs, labels, steps, x_dim):
    n_samples = len(seqs)
    max_len = steps
    x = numpy.zeros((max_len, n_samples, x_dim)).astype('float32')

    for idx, s in enumerate(seqs):
        x[:,idx,:] = s

    return x
