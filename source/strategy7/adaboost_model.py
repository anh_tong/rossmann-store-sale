__author__ = 'anhth'

from model import Model
from sklearn.ensemble import AdaBoostRegressor

class AdaBoostModel(Model):

    def __init__(self, train, test, features):
        self.train = train
        self.test = test
        self.features = features
        self.regressor = AdaBoostRegressor()

    def name(self):
        return "ADABOOST"

    def fit(self):
        return

    def predict_on_test(self):
        raise NotImplementedError

    def predict_on_train(self):
        return

