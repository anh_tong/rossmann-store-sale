__author__ = 'anhth'

import pandas as pd

def build_feature(data):

    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1


    data.loc[:, "year"] = data.Date.dt.year
    data.loc[:, "month"] = data.Date.dt.month
    data.loc[:, "day"] = data.Date.dt.year
    data.loc[:, 'DayOfWeek'] = data.Date.dt.dayofweek



    return

if __name__ == "__main__":

    print 'Loading train, test, store files ...'
    train = pd.read_csv("../../data/train.csv", parse_dates=[2])
    test = pd.read_csv("../../data/test.csv", parse_dates=[3])
    store = pd.read_csv("../../data/store.csv")

    train.fillna(0, inplace=True)
    test.fillna(0, inplace=True)


    train = train[train["Open"] != 0]
    train = train[train["Sales"] > 0]
    test = test[test["Open"] != 0]

    train = pd.merge(train, store, on='Store')
    test = pd.merge(test, store, on='Store')
    test.loc[test["Store"] == 622 & test.Open.isnull(), 'Open'] = 0









