__author__ = 'anhth'



class Model(object):

    def name(self):
        raise NotImplementedError

    def fit(self):
        raise NotImplementedError

    def predict_on_test(self):
        raise NotImplementedError

    def predict_on_train(self):
        raise NotImplementedError
