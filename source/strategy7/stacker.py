__author__ = 'anhth'

from sklearn.ensemble import GradientBoostingRegressor
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import make_scorer
import pandas as pd
import numpy as np
from operator import itemgetter


def rmspe(y, y_pred, **kwargs):
    return - np.sqrt(np.mean((y_pred/y-1) ** 2))


def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")


class Stacker:

    def __init__(self, models = None, params = None):
        if models is None:
            self.models = []
        else:
            self.models = models

        if params is None:
            self.params = {
            "n_estimators": [10, 50, 130, 150, 200, 250],
            "max_depth": [3, 4,5,6],
            "max_features":["auto","sqrt"],
            "bootstrap": [True, False]
            }
        else:
            self.params = params

        self.regressor = GradientBoostingRegressor()


    def set_real_data(self, real_data):
        self.real_data = real_data


    def add_model(self, model):
        self.models.append(model)


    def fit(self):
        print("Fitting Stacker ...")

        stack_data = pd.DataFrame()

        for m in self.models:
            stack_data[m.name()] = m.predict_on_train()

        mse = make_scorer(rmspe)

        self.grid_search = GridSearchCV(self.regressor,
                                   param_grid=self.params,
                                   verbose= 1,
                                   scoring= mse,
                                   n_jobs= 8
                                   ).fit(stack_data, self.real_data)


        print("Best score: %0.3f" % self.grid_search.best_score_)
        print(self.grid_search.best_estimator_)
        report(self.grid_search.grid_scores_)


    def predict(self):
        print("Predicting Stacker ...")
        stack_data = pd.DataFrame()
        for m in self.models:
            stack_data[m.name()] = m.predict_on_test()


        return self.grid_search.best_estimator_.predict(stack_data)









