__author__ = 'anhth'

from model import Model
from xgboost import XGBRegressor
import numpy as np
from sklearn.cross_validation import train_test_split
import xgboost as xgb

def rmspe(y, yhat):
    return np.sqrt(np.mean((yhat/y-1) ** 2))

def rmspe_xg(yhat, y):
    y = np.expm1(y.get_label())
    yhat = np.expm1(yhat)
    return "rmspe", rmspe(y,yhat)

class XgboostModel(Model, object):

    def __init__(self, train, test, features):

        self.params = {"objective": "reg:linear",
          "eta": 0.03,
          "max_depth": 10,
          "subsample": 0.7,
          "colsample_bytree": 0.7,
          "silent": 1,
          "seed": 1301
          }

        self.features = features

        self.train = train
        self.test = test
        self.regressor = XGBRegressor()

    def name(self):
        return "XGBOOST"

    def fit(self):
        num_boost_round = 3000


        X_train, X_valid = train_test_split(self.train, test_size=0.012)
        y_train = np.log1p(X_train.Sales)
        y_valid = np.log1p(X_valid.Sales)
        dtrain = xgb.DMatrix(X_train[features], y_train)
        dvalid = xgb.DMatrix(X_valid[features], y_valid)
        watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
        gbm = xgb.train(self.params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=100, \
          feval=rmspe_xg)

    def predict_on_test(self):
        raise NotImplementedError

    def predict_on_train(self):
        raise NotImplementedError

