__author__ = 'anhth'


import pandas as pd
from tranformer import transform_test, transform_train
from patsy import dmatrices
import numpy as np
import statsmodels.api as sm
from sklearn.cross_validation import train_test_split
import xgboost as xgb
import time
from sklearn.preprocessing import StandardScaler
import operator
import matplotlib.pyplot as plt


import logging
logging.basicConfig(filename='example.log',level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

def rmspe(y, yhat):
    return np.sqrt(np.mean((yhat/y-1) ** 2))

def rmspe_xg(yhat, y):
    y = y.get_label()
    return "rmspe", rmspe(y,yhat)

class Fitter:


    def __init__(self, name, train, test, store):
        self.name = name
        self.train = train
        self.test = test
        self.store = store




    def fit(self):
        scaler = StandardScaler()

        t1 = time.clock()
        features, train = transform_train(self.train, self.store)
        feature, test = transform_test(self.test, self.store)

        X_train, X_valid = train_test_split(train, test_size=0.05)
        # y_train = np.log1p(X_train.Sales)
        # y_valid = np.log1p(X_valid.Sales)
        y_train = X_train.Sales
        y_valid = X_valid.Sales
        dtrain = xgb.DMatrix(X_train[features], y_train)
        dvalid = xgb.DMatrix(X_valid[features], y_valid)

        params = {"objective": "reg:linear",
          "eta": 0.003,
          "max_depth": 7,
          "subsample": 0.7,
          "colsample_bytree": 0.7,
          }
        num_boost_round = 2000


        watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
        gbm = xgb.train(params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=100, \
          feval=rmspe_xg)


        yhat = gbm.predict(xgb.DMatrix(X_valid[features]))
        error = rmspe(X_valid.Sales.values, yhat)
        #
        #
        # dtest = xgb.DMatrix(test[features])
        # test_probs = gbm.predict(dtest)
        # # Make Submission
        # result = pd.DataFrame({"Id": test["Id"], 'Sales': test_probs})
        # file_to_save = "./store/" + str(self.store) +".csv"
        # result.to_csv(file_to_save, index=False)
        t1 = time.clock() - t1
        #
        # #
        # # ceate_feature_map(features)
        # # importance = gbm.get_fscore(fmap='xgb.fmap')
        # # importance = sorted(importance.items(), key=operator.itemgetter(1))
        # #
        # # df = pd.DataFrame(importance, columns=['feature', 'fscore'])
        # # df['fscore'] = df['fscore'] / df['fscore'].sum()
        # #
        # # featp = df.plot(kind='barh', x='feature', y='fscore', legend=False, figsize=(6, 10))
        # # plt.title('XGBoost Feature Importance')
        # # plt.xlabel('relative importance')
        # # fig_featp = featp.get_figure()
        # # fig_featp.savefig('./features/feature_importance_xgb_%d.png'%(self.store),bbox_inches='tight',pad_inches=1)
        #
        logging.info('Store ' + str(self.store) + '\tRMSPE: {:.6f}'.format(error) + "\telapsed:%d"%t1)

def ceate_feature_map(features):
    outfile = open('xgb.fmap', 'w')
    for i, feat in enumerate(features):
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
    outfile.close()

from operator import itemgetter
def report(grid_scores, n_top=3):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")