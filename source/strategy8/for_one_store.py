__author__ = 'anhth'

import pandas as pd
import numpy as np
import time

import statsmodels.api as sm
import matplotlib.pyplot as plt

def transform_train(data, store=None):

    """
    Transform train data
    :param data: input train data for one store
    :type data: DataFrame
    :return: A transformed data frame
    :rtype: DataFrame
    """

    data = data[data['Open'] != 0]

    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1

    return data

if __name__ == "__main__":
    train = pd.read_csv("../../data/train.csv", parse_dates = [2])
    test = pd.read_csv("../../data/test.csv", parse_dates = [3])
    store = train['Store']
    store = store.drop_duplicates()

    num_empty = 0
    id = []
    sales = []
    final_result = None
    start_time = time.time()
    for s in store.values:
        if s == 1:
            continue
        t1 = time.time()
        print("===============================================")
        print("                     %d                        " % s)
        print("===============================================")


        data = transform_train(train[train['Store'] == s])
        data.loc[:, "Date"] = data["Date"].convert_objects(convert_dates='coerce')
        data = data.sort_index(by = "Date")
        data = data.set_index("Date")

        data['Natural Log'] = data['Sales'].apply(lambda  x: np.log(x))

        model = sm.tsa.ARIMA(data['Sales'].iloc[1:], order=(30, 0, 0), freq= '1d')

        results = model.fit()
        data['Forecast'] = results.fittedvalues
        data[['Sales', 'Forecast']].plot(figsize=(16, 12))
        print results.forecast(20)
        # data.index = pd.Index(data.Date)
        #
        # del data["DayOfWeek"]
        # del data["Store"]
        # del data["Customers"]
        # del data["Open"]
        # del data["Promo"]
        # del data["StateHoliday"]
        # del data["SchoolHoliday"]
        # del data["Date"]
        #
        # arma_mod20 = sm.tsa.ARMA(data, (3,0), ).fit()
        #
        # predicted = arma_mod20.predict(exog=dict(x1=["2015-08-01", "2015-08-02"]), dynamic=True)
        # print(predicted)

        t2 =  time.time() - t1
        print("Store %d --- Time: %d" %(s, t2))
        break

    plt.show()

    print("The number of store which is not predicted for is %d" % num_empty)

    pd.DataFrame({"Id": id, "Sales":sales}).to_csv("../../result/submission-21-11-2015-3.csv",  index_label=None,index_col=False,index=False)
    elapsed_time = time.time() - start_time
    print("Total time is %d" % elapsed_time)
    print "Done !!!!!!!!!!!"


