__author__ = 'anhth'


import pandas as pd
from fitter import Fitter
import datetime
import time
import numpy as np


if __name__ == "__main__":

    train = pd.read_csv("../../data/train.csv", parse_dates = [2])
    test = pd.read_csv("../../data/test.csv", parse_dates = [3])
    real_store = pd.read_csv("../../data/store.csv")
    trend = pd.read_csv("../../data/daily_google_trends.csv", parse_dates = [1])
    trend['Date'] = trend.Day

    train = pd.merge(train, real_store, on='Store')
    test = pd.merge(test, real_store, on='Store')
    test.loc[test["Store"] == 622 & test.Open.isnull(), 'Open'] = 0
    test.loc[test.Open.isnull(), 'Open'] = 1
    train.loc[train.Open.isnull(), 'Open'] = 1
    # train.fillna(1, inplace=True)
    # test.fillna(1, inplace=True)

    train = train[train["Open"] != 0]
    train = train[train["Sales"] > 0]
    test = test[test["Open"] != 0]



    store = train['Store']
    store = store.drop_duplicates()
    num_empty = 0
    id = []
    sales = []
    final_result = None
    start_time = time.time()
    for s in store.values:
        print("===============================================")
        print("                     %d                        " % s)
        print("===============================================")
        test_s = test[test['Store'] == s]
        if test_s.empty:
            num_empty += 1
        else:
            temp_store = real_store[real_store['Store'] == s]
            train_s = train[train['Store'] == s]
            train_s = pd.merge(train_s, trend, how='left', on = ["Date"] )
            fitter = Fitter(name=str(s), train=train_s, test= test_s, store = s)
            fitter.fit()
            # if (final_result is None):
            #     final_result = ret_df
            # else:
            #     final_result.append(ret_df)


    print("The number of store which is not predicted for is %d" % num_empty)

    pd.DataFrame({"Id": id, "Sales":sales}).to_csv("../../result/submission-21-11-2015-3.csv",  index_label=None,index_col=False,index=False)
    elapsed_time = time.time() - start_time
    print("Total time is %d" % elapsed_time)
    print "Done !!!!!!!!!!!"



