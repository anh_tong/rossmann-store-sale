__author__ = 'anhth'

from pandas import DataFrame
import pandas as pd
from calendar import month_abbr


def create_month_dict():
    month_dict = dict()
    month_dict[1] = "Jan"
    month_dict[2] = "Feb"
    month_dict[3] = "Mar"
    month_dict[4] = "Apr"
    month_dict[5] = "May"
    month_dict[6] = "Jun"
    month_dict[7] = "Jul"
    month_dict[8] = "Aug"
    month_dict[9] = "Sept"
    month_dict[10] = "Oct"
    month_dict[11] = "Nov"
    month_dict[12] = "Dec"
    return month_dict


def toBinary(featureCol, df):
    values = set(df[featureCol].unique())
    newCol = [featureCol + val for val in values]
    for val in values:
        df[featureCol + val] = df[featureCol].map(lambda x: 1 if x == val else 0)
    return newCol

def transform_train(data, store=None):

    """
    Transform train data
    :param data: input train data for one store
    :type data: DataFrame
    :return: A transformed data frame
    :rtype: DataFrame
    """


    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1

    features = []

    features.extend(['Promo', 'Promo2', 'daily_rossmann_DE', 'daily_rossmann_DE-SN', 'daily_rossmann_DE-RP', 'daily_rossmann_DE-BE', 'daily_rossmann_DE-HB', 'daily_rossmann_DE-BW',
                     'daily_rossmann_DE-ST', 'daily_rossmann_DE-NI', 'daily_rossmann_DE-SH', 'daily_rossmann_DE-TH', 'daily_rossmann_DE-HE', 'daily_rossmann_DE-HH', 'daily_rossmann_DE-BY',
                     'daily_rossmann_DE-NW'
                     ])

    features.append('SchoolHoliday')
    data['SchoolHoliday'] = data['SchoolHoliday'].astype(float)
    features.append("SchoolHolidayPromo")

    def g(row):
       if row['SchoolHoliday'] == 1 and row['Promo'] == 1:
           return 4
       elif row['Promo'] == 1:
           return 3
       elif row['SchoolHoliday'] == 1:
           return 2
       else:
           return 1

    data['SchoolHolidayPromo'] = data.apply(g, axis=1)

    features.append('DayOfWeek')
    features.append('month')
    features.append('day')
    features.append('year')
    features.append('week')
    data['year'] = data.Date.dt.year
    data['month'] = data.Date.dt.month
    data['day'] = data.Date.dt.day
    data['DayOfWeek'] = data.Date.dt.dayofweek
    data.loc[:,'week'] = data.Date.dt.week


    features.append('CompetitionOpen')
    data['CompetitionOpen'] = 12 * (data.year - data.CompetitionOpenSinceYear) + \
	(data.month - data.CompetitionOpenSinceMonth)
    data['CompetitionOpen'] = data.CompetitionOpen.apply(lambda x: 1 if x > 0 else 0)

    features.append("PromoOpen")
    data['PromoOpen'] = 12 * (data.year - data.Promo2SinceYear) + \
    (data.week - data.Promo2SinceWeek) / float(4)
    data['PromoOpen'] = data.PromoOpen.apply(lambda x: x if x > 0 else 0)
    month_dict = create_month_dict()
    def f(row):
        if row['Open'] > 0:
            if row["PromoInterval"] == 0:
                return 0
            if month_dict[row['month']] in row["PromoInterval"]:
                return 1
            else:
                return 0
        else:
            return 0


    data.loc[:, 'PromoOpen'] = data.apply(f, axis = 1)


    # for x in ['a', 'b', 'c', 'd']:
    #     features.append('StoreType' + x)
    #     data['StoreType' + x] = data['StoreType'].map(lambda y: 1 if y == x else 0)
    #
    # newCol = toBinary('Assortment', data)
    # features += newCol

    return features, data


def transform_test(data, store = None):

    """
    Transform train data
    :param data: input train data for one store
    :type data: DataFrame
    :return: A transformed data frame
    :rtype: DataFrame
    """

    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1

    features = []

    features.extend(['Promo', 'Promo2'])

    features.append('SchoolHoliday')
    data['SchoolHoliday'] = data['SchoolHoliday'].astype(float)
    features.append("SchoolHolidayPromo")
    def g(row):
       if row['SchoolHoliday'] == 1 and row['Promo'] == 1:
           return 4
       elif row['Promo'] == 1:
           return 3
       elif row['SchoolHoliday'] == 1:
           return 2
       else:
           return 1

    data['SchoolHolidayPromo'] = data.apply(g, axis=1)

    features.append('DayOfWeek')
    features.append('month')
    features.append('day')
    features.append('year')
    features.append('week')
    data['year'] = data.Date.dt.year
    data['month'] = data.Date.dt.month
    data['day'] = data.Date.dt.day
    data['DayOfWeek'] = data.Date.dt.dayofweek
    data.loc[:,'week'] = data.Date.dt.week


    features.append('CompetitionOpen')
    data['CompetitionOpen'] = 12 * (data.year - data.CompetitionOpenSinceYear) + \
	(data.month - data.CompetitionOpenSinceMonth)
    data['CompetitionOpen'] = data.CompetitionOpen.apply(lambda x: 1 if x > 0 else 0)

    features.append("PromoOpen")
    data['PromoOpen'] = 12 * (data.year - data.Promo2SinceYear) + \
    (data.week - data.Promo2SinceWeek) / float(4)
    data['PromoOpen'] = data.PromoOpen.apply(lambda x: x if x > 0 else 0)
    month_dict = create_month_dict()
    def f(row):
        if row['Open'] > 0:
            if row["PromoInterval"] == 0:
                return 0
            if month_dict[row['month']] in row["PromoInterval"]:
                return 1
            else:
                return 0
        else:
            return 0


    data.loc[:, 'PromoOpen'] = data.apply(f, axis = 1)
    #
    #
    # for x in ['a', 'b', 'c', 'd']:
    #     features.append('StoreType' + x)
    #     data['StoreType' + x] = data['StoreType'].map(lambda y: 1 if y == x else 0)
    #
    # newCol = toBinary('Assortment', data)
    # features += newCol

    return features, data