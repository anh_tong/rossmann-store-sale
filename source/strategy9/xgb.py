__author__ = 'anhth'


import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
import xgboost as xgb
import operator
import matplotlib
matplotlib.use("Agg") #Needed to save figures
import matplotlib.pyplot as plt


def create_month_dict():
    month_dict = dict()
    month_dict[1] = "Jan"
    month_dict[2] = "Feb"
    month_dict[3] = "Mar"
    month_dict[4] = "Apr"
    month_dict[5] = "May"
    month_dict[6] = "Jun"
    month_dict[7] = "Jul"
    month_dict[8] = "Aug"
    month_dict[9] = "Sept"
    month_dict[10] = "Oct"
    month_dict[11] = "Nov"
    month_dict[12] = "Dec"
    return month_dict



def ceate_feature_map(features):
    outfile = open('xgb.fmap', 'w')
    for i, feat in enumerate(features):
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
    outfile.close()

def rmspe(y, yhat):
    return np.sqrt(np.mean((yhat/y-1) ** 2))

def rmspe_xg(yhat, y):
    y = np.expm1(y.get_label())
    yhat = np.expm1(yhat)
    return "rmspe", rmspe(y,yhat)

def toBinary(featureCol, df):
    values = set(df[featureCol].unique())
    newCol = [featureCol + val for val in values]
    for val in values:
        df[featureCol + val] = df[featureCol].map(lambda x: 1 if x == val else 0)
    return newCol

# Gather some features
def build_features(features, data):
    # remove NaNs
    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1
    # Use some properties directly
    # features.extend(['Store', 'CompetitionDistance', 'Promo', 'Promo2'])
    # features.extend(['Store', 'CompetitionDistance', 'CompetitionOpenSinceMonth',
    #                 'CompetitionOpenSinceYear', 'Promo', 'Promo2', 'Promo2SinceWeek', 'Promo2SinceYear'])

    features.extend(['Store', 'CompetitionDistance', 'Promo', 'Promo2', 'Promo2SinceWeek', 'Promo2SinceYear',

                     # , 'daily_rossmann_DE-BE', 'daily_rossmann_DE-HB', 'daily_rossmann_DE-BW',
                     # 'daily_rossmann_DE-ST', 'daily_rossmann_DE-NI', 'daily_rossmann_DE-SH', 'daily_rossmann_DE-TH', 'daily_rossmann_DE-HE', 'daily_rossmann_DE-HH', 'daily_rossmann_DE-BY',
                     # 'daily_rossmann_DE-NW'

                     ])

    # add some more with a bit of preprocessing
    features.append('SchoolHoliday')
    data['SchoolHoliday'] = data['SchoolHoliday'].astype(float)
    # features.append("SchoolHolidayPromo")
    # data['SchoolHolidayPromo'] = data['SchoolHoliday']*data['Promo']
    # features.append('StateHoliday')
    # data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    # data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    # data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    # data['StateHoliday'] = data['StateHoliday'].astype(float)

    def state(row):
        if row["State"] == 'HB,NI':
            return (row["daily_rossmann_DE-HB"] + row["daily_rossmann_DE-NI"])*1.0/2
        else:
            row_name = "daily_rossmann_DE-{}".format(row["State"])
            return row[row_name]
    data["trend"] = data.apply(state, axis = 1)
    features.append("trend")
    newCol1 = toBinary('State', data)
    features += newCol1

    features.append('DayOfWeek')
    features.append('month')
    features.append('day')
    features.append('year')
    data['year'] = data.Date.dt.year
    data['month'] = data.Date.dt.month
    data['day'] = data.Date.dt.day
    data['DayOfWeek'] = data.Date.dt.dayofweek
    data.loc[:,'week'] = data.Date.dt.week


    features.append('CompetitionOpen')
    data['CompetitionOpen'] = 12 * (data.year - data.CompetitionOpenSinceYear) + \
	(data.month - data.CompetitionOpenSinceMonth)
    data['CompetitionOpen'] = data.CompetitionOpen.apply(lambda x: 1 if x > 0 else 0)

    features.append("PromoOpen")
    data['PromoOpen'] = 12 * (data.year - data.Promo2SinceYear) + \
	(data.week - data.Promo2SinceWeek) / float(4)
    data['PromoOpen'] = data.PromoOpen.apply(lambda x: x if x > 0 else 0)
    month_dict = create_month_dict()
    def f(row):
        if row['Open'] > 0:
            if row["PromoInterval"] == 0:
                return 0
            if month_dict[row['month']] in row["PromoInterval"]:
                return 1
            else:
                return 0
        else:
            return 0


    data.loc[:, 'PromoOpen'] = data.apply(f, axis = 1)


    for x in ['a', 'b', 'c', 'd']:
        features.append('StoreType' + x)
        data['StoreType' + x] = data['StoreType'].map(lambda y: 1 if y == x else 0)

    newCol = toBinary('Assortment', data)
    features += newCol

## Start of main script

print("Load the training, test and store data using pandas")
train = pd.read_csv("../../data/train.csv", parse_dates=[2])
test = pd.read_csv("../../data/test.csv", parse_dates=[3])
store = pd.read_csv("../../data/store.csv")
store_states = pd.read_csv("../../data/store_states.csv")
store = pd.merge(store, store_states, how='left', on = ["Store"])
trend = pd.read_csv("../../data/daily_google_trends.csv", parse_dates = [1])
trend["Date"] = trend.Day
train = pd.merge(train, trend, how='left', on=["Date"])
test = pd.merge(test, trend, how='left', on=["Date"])
test['StateBE'] = 0
test['StateST'] = 0
test['StateSN'] = 0
test['StateTH'] = 0


print("Assume store open, if not provided")
test.loc[test.Open.isnull() & test['Store'] == 622, "Open"] = 0
test = test[test["Open"] != 0]

train.fillna(1, inplace=True)
test.fillna(1, inplace=True)

print("Consider only open stores for training. Closed stores wont count into the score.")
train = train[train["Open"] != 0]
print("Use only Sales bigger then zero. Simplifies calculation of rmspe")
train = train[train["Sales"] > 0]


test_not_open = test[test["Open"] == 0]



print("Join with store")
train = pd.merge(train, store, on='Store')
test = pd.merge(test, store, on='Store')

features = []

print("augment features")
build_features(features, train)
build_features([], test)
print(features)

print('training data processed')

params = {"objective": "reg:linear",
           "booster": "gbtree",
          "eta": 0.05,
          "max_depth": 14,
          "subsample": 0.9,
          "colsample_bytree": 0.7,
          "silent": 1,
          # "seed": 1301
          "alpha": 0.5,
          "lambda": 0.5
          }
num_boost_round = 400
print("Train a XGBoost model")
X_train, X_valid = train_test_split(train, test_size=0.012)
y_train = np.log1p(X_train.Sales)
y_valid = np.log1p(X_valid.Sales)
dtrain = xgb.DMatrix(X_train[features], y_train)
dvalid = xgb.DMatrix(X_valid[features], y_valid)

watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
gbm = xgb.train(params, dtrain, num_boost_round, evals=watchlist, early_stopping_rounds=100, \
  feval=rmspe_xg)

print("Validating")
yhat = gbm.predict(xgb.DMatrix(X_valid[features]))
error = rmspe(X_valid.Sales.values, np.expm1(yhat))
print('RMSPE: {:.6f}'.format(error))

print("Make predictions on the test set")
dtest = xgb.DMatrix(test[features])
test_probs = gbm.predict(dtest)
# Make Submission
result = pd.DataFrame({"Id": test["Id"], 'Sales': np.expm1(test_probs)})
result.to_csv("xgboost_10_submission_02-12-1.csv", index=False)

# XGB feature importances
# Based on https://www.kaggle.com/mmueller/liberty-mutual-group-property-inspection-prediction/xgb-feature-importance-python/code

ceate_feature_map(features)
importance = gbm.get_fscore(fmap='xgb.fmap')
importance = sorted(importance.items(), key=operator.itemgetter(1))

df = pd.DataFrame(importance, columns=['feature', 'fscore'])
df['fscore'] = df['fscore'] / df['fscore'].sum()

featp = df.plot(kind='barh', x='feature', y='fscore', legend=False, figsize=(6, 10))
plt.title('XGBoost Feature Importance')
plt.xlabel('relative importance')
fig_featp = featp.get_figure()
fig_featp.savefig('feature_importance_xgb.png',bbox_inches='tight',pad_inches=1)

