__author__ = 'anhth'


import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
import xgboost as xgb
import operator
import matplotlib
matplotlib.use("Agg") #Needed to save figures
import matplotlib.pyplot as plt
import warnings
from sklearn.linear_model import (RandomizedLasso, lasso_stability_path,
                                  LassoLarsCV)
from sklearn.utils import ConvergenceWarning
from sklearn.metrics import auc, precision_recall_curve



def ceate_feature_map(features):
    outfile = open('xgb.fmap', 'w')
    for i, feat in enumerate(features):
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
    outfile.close()

def rmspe(y, yhat):
    return np.sqrt(np.mean((yhat/y-1) ** 2))

def rmspe_xg(yhat, y):
    y = np.expm1(y.get_label())
    yhat = np.expm1(yhat)
    return "rmspe", rmspe(y,yhat)

def toBinary(featureCol, df):
    values = set(df[featureCol].unique())
    newCol = [featureCol + val for val in values]
    for val in values:
        df[featureCol + val] = df[featureCol].map(lambda x: 1 if x == val else 0)
    return newCol

# Gather some features
def build_features(features, data):
    # remove NaNs
    data.fillna(0, inplace=True)
    data.loc[data.Open.isnull(), 'Open'] = 1
    # Use some properties directly
    features.extend(['Store', 'CompetitionDistance', 'CompetitionOpenSinceMonth',
                    'CompetitionOpenSinceYear', 'Promo', 'Promo2', 'Promo2SinceWeek', 'Promo2SinceYear'])

    # add some more with a bit of preprocessing
    features.append('SchoolHoliday')
    data['SchoolHoliday'] = data['SchoolHoliday'].astype(float)

    # features.append('StateHoliday')
    # data.loc[data['StateHoliday'] == 'a', 'StateHoliday'] = '1'
    # data.loc[data['StateHoliday'] == 'b', 'StateHoliday'] = '2'
    # data.loc[data['StateHoliday'] == 'c', 'StateHoliday'] = '3'
    # data['StateHoliday'] = data['StateHoliday'].astype(float)

    features.append('DayOfWeek')
    features.append('month')
    features.append('day')
    features.append('year')
    data['year'] = data.Date.dt.year
    data['month'] = data.Date.dt.month
    data['day'] = data.Date.dt.day
    data['DayOfWeek'] = data.Date.dt.dayofweek

    features.append('weekday')
    data['weekday'] = data.Date.dt.weekday

    for x in ['a', 'b', 'c', 'd']:
        features.append('StoreType' + x)
        data['StoreType' + x] = data['StoreType'].map(lambda y: 1 if y == x else 0)

    newCol = toBinary('Assortment', data)
    features += newCol

## Start of main script

print("Load the training, test and store data using pandas")
train = pd.read_csv("../data/train.csv", parse_dates=[2])
test = pd.read_csv("../data/test.csv", parse_dates=[3])
store = pd.read_csv("../data/store.csv")

print("Assume store open, if not provided")
train.fillna(1, inplace=True)
test.fillna(1, inplace=True)

print("Consider only open stores for training. Closed stores wont count into the score.")
train = train[train["Open"] != 0]
print("Use only Sales bigger then zero. Simplifies calculation of rmspe")
train = train[train["Sales"] > 0]

print "Before ", len(test[test["Open"] != 0].index)
test.loc[test.Open.isnull() & test['Store'] == 622, "Open"] = 0
print "After ", len(test[test["Open"] != 0].index)
test = test[test["Open"] != 0]

test_not_open = test[test["Open"] == 0]

print("Join with store")
train = pd.merge(train, store, on='Store')
test = pd.merge(test, store, on='Store')

features = []

print("augment features")
build_features(features, train)
build_features([], test)
print(features)

print('training data processed')

params = {"objective": "reg:linear",
          "eta": 0.02,
          "max_depth": 12,
          "subsample": 0.9,
          "colsample_bytree": 0.7,
          # "silent": 1,
          # "seed": 1301
          }
num_boost_round = 3000

print("Train a XGBoost model")
X_train, X_valid = train_test_split(train, test_size=0.012)
y_train = np.log1p(X_train.Sales)
y_valid = np.log1p(X_valid.Sales)

with warnings.catch_warnings():
    warnings.simplefilter('ignore', UserWarning)
    warnings.simplefilter('ignore', ConvergenceWarning)
    lars_cv = LassoLarsCV(cv=6).fit(X_train[features], y_train)

    # Run the RandomizedLasso: we use a paths going down to .1*alpha_max
    # to avoid exploring the regime in which very noisy variables enter
    # the model
    alphas = np.linspace(lars_cv.alphas_[0], .1 * lars_cv.alphas_[0], 6)
    clf = RandomizedLasso(alpha=alphas, random_state=42).fit(X_train[features], y_train)
print np.abs(lars_cv.coef_)
plt.figure()
#
for name, score in [
                        ('Stability selection', clf.scores_),
                        ('Lasso coefs', np.abs(lars_cv.coef_))
                        ]:
        precision, recall, thresholds = precision_recall_curve(coef != 0,
                                                               score)
#         plt.semilogy(np.maximum(score / np.max(score), 1e-4),
#                      label="%s. AUC: %.3f" % (name, auc(recall, precision)))
#
#     # plt.plot(np.where(coef != 0)[0], [2e-4] * n_relevant_features, 'mo',
#     #          label="Ground truth")
#     plt.xlabel("Features")
#     plt.ylabel("Score")
#     # Plot only the 100 first coefficients
#     plt.xlim(0, 100)
#     plt.legend(loc='best')
#     plt.title('Feature selection scores - Mutual incoherence: %.1f'
#               % mi)
#
# plt.show()